const formEl = document.getElementById('best-books-form');
const yearEl = document.getElementById('year');
const monthEl = document.getElementById('month');
const dateEl = document.getElementById('date');

formEl.addEventListener('submit', function (e) {
  e.preventDefault();

  const year = yearEl.value;
  const month = monthEl.value;
  const date = dateEl.value;
  const ymd = year + "-" + month + "-" + date;
  const list = 'hardcover-fiction';
  const url = `https://api.nytimes.com/svc/books/v3/lists/${ymd}/${list}.json?offset=0&api-key=${apiKey}`;




  // Fetch bestselling books for date and add top 5 to page

  //https://api.nytimes.com/svc/books/v3/lists/2020-03-02/hardcover-fiction.json?




  // Make a request for a user with a given ID
  axios.get(url)
    .then(function (response) {
      // handle success
      let output = document.getElementById("books-container");
      for (i = 0; i < response.data.results.books.length; i++) {
        if (i == 5) {
          break;
        }
        let currentBook = response.data.results.books[i];
        console.log(currentBook);
        let title = document.createElement('div');
        let author = document.createElement('div');
        let description = document.createElement('div');
        title.innerHTML = currentBook.title;
        author.innerHTML = currentBook.author;
        description.innerHTML = currentBook.description;
        output.appendChild(title);
        output.appendChild(author);
        output.appendChild(description);
        output.appendChild(document.createElement('br'))
      }
      console.log(response);
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });
});