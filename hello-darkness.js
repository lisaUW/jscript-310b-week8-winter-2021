/*
Using setInterval:
1. Every half a second, darken the body backgroundColor by 1
2. Stop (clear interval) when you get to zero .

Starting <body> background-color:
*/

let color = 255;

function changebackground() {
    if (color == 0) {
        clearInterval(loop);
    } else {
        color--
        let backgroundColor = `rgb(${color}, ${color}, ${color})`;
        document.body.style.backgroundColor = backgroundColor;
    }
}
let loop = setInterval(changebackground, 50);


/*
element.style.backgroundColor = 'rgb(255, 255, 255)';
After 0.5 second, background-color:

element.style.backgroundColor = 'rgb(254, 254, 254)';
Promise
1. Create a new promise. The function passed to the promise should:

After 1 second, call Math.random()
If the result of Math.random() is > 0.5, call resolve()
If the result of Math.random() is <= 0.5, call reject()
2. If the promise is resolved, should console.log('success')

3. If the promise is rejected, should console.log('fail')

4. In either case, should console.log('complete')
*/