/*
1. Using requestAnimationFrame, animate the background-color of the body 
so that it changes from black rgb(0, 0, 0) to white rgb(255, 255, 255).  
It should increment by 1every frame.

Once the background-color is rgb(255, 255, 255), should not call 
requestAnimationFrame again.
It should take approximately 4-5 seconds for the background to 
animate from black to white.


*/


const body = document.body;
let color = 0;

function changeColorAnimation(timestamp) {
    color++;
    let backgroundColor = `rgb(${color}, ${color}, ${color})`;
    document.body.style.backgroundColor = backgroundColor;

    if (color < 255) { // Stop the animation after 2 seconds
        window.requestAnimationFrame(changeColorAnimation);
    }
}

window.requestAnimationFrame(changeColorAnimation);

///////

/*

function changebackground() {
    if (color == 0) {
        clearInterval(loop);
    } else {

    }
}
let loop = setInterval(changebackground, 50);
*/